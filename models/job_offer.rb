class JobOffer
  include ActiveModel::Validations

  attr_accessor :id, :user, :user_id, :title, :location, :description,
                :is_active, :experience_required, :updated_on, :created_on

  validates :title, presence: true
  validates :experience_required, numericality: { only_integer: true, greater_than_or_equal_to: 0 },
                                  allow_blank: true, allow_nil: true

  def initialize(data = {}) # rubocop:disable Metrics/AbcSize
    @id = data[:id]
    @title = data[:title]
    @location = data[:location]
    @description = data[:description]
    @is_active = data[:is_active]
    @updated_on = data[:updated_on]
    @created_on = data[:created_on]
    @user_id = data[:user_id]
    @experience_required = data[:experience_required] unless data[:experience_required].blank?
    validate!
  end

  def owner
    user
  end

  def owner=(a_user)
    self.user = a_user
  end

  def activate
    self.is_active = true
  end

  def deactivate
    self.is_active = false
  end

  def old_offer?
    (Date.today - updated_on) >= 30
  end
end
