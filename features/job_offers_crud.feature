Feature: Job Offers CRUD
  In order to get employees
  As a job offerer
  I want to manage my offers

  Background:
  	Given I am logged in as job offerer

  Scenario: Create new offer
    When I create a new offer with "Programmer vacancy" as the title
    Then I should see a offer created confirmation message
    And I should see "Programmer vacancy" in my offers list

  Scenario: Update offer
    Given I have "Programmer vacancy" offer in my offers list
    When I change the title to "Programmer vacancy!!!"
    Then I should see a offer updated confirmation message
    And I should see "Programmer vacancy!!!" in my offers list

  Scenario: Delete offer
    Given I have "Programmer vacancy" offer in my offers list
    When I delete it
    Then I should see a offer deleted confirmation message
    And I should not see "Programmer vacancy!!!" in my offers list

  Scenario: Create new offer specifying years of experience required
    When I create a new offer with "InVision Dev vacancy" as the title and "3" as the experience required
    Then I should see a offer created confirmation message
    And I should see "3" in the experience required of this offer in my offers list

 Scenario: Create new offer specifying 0 years of experience required
    When I create a new offer with "InVision Dev vacancy" as the title and "0" as the experience required
    Then I should see a offer created confirmation message
    And I should see "Not specified" in the experience required of this offer in my offers list

  Scenario: Create new offer without specifying experience required
    When I create a new offer with "InVision Dev vacancy" as the title
    Then I should see a offer created confirmation message
    And I should see "Not specified" in the experience required of this offer in my offers list

  Scenario: Observe created offer in Job Offer list
    Given I have "Programmer vacancy" offer in my offers list
    When I activate it
    Then I should see an offer activated confirmation message
    And I should see the offer in Job offers list

  Scenario: Observe offer experience required specified in Job Offer list
    Given I have "Programmer vacancy" offer with "5" as experience required in my offers list
    When I activate it
    Then I should see an offer activated confirmation message
    And I should see the offer in Job offers list with "5" as the experience required

  Scenario: Observe offer experience not specified in Job Offer list
    Given I have "Programmer vacancy" offer without specifying the experience required in my offers list
    When I activate it
    Then I should see an offer activated confirmation message
    And I should see the offer in Job offers list with "Not specified" as the experience required

  Scenario: Observe offer experience required "Not specified" when exp. is 0 in Job Offer list
    Given I have "Programmer vacancy" offer with "0" as experience required in my offers list
    When I activate it
    Then I should see an offer activated confirmation message
    And I should see the offer in Job offers list with "Not specified" as the experience required