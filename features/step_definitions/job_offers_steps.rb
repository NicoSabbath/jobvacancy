OFFER_CREATED_MESSAGE = 'Offer created'.freeze
OFFER_UPDATED_MESSAGE = 'Offer updated'.freeze
OFFER_DELETED_MESSAGE = 'Offer deleted'.freeze
OFFER_ACTIVATED_MESSAGE = 'Offer activated'.freeze

REGISTRATION_MENU = 'register'.freeze
JOB_OFFERS_MENU = 'Job offers'.freeze

When(/^I browse the default page$/) do
  visit '/'
end

Given(/^I am logged in as job offerer$/) do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  page.should have_content('offerer@test.com')
end

When(/^I create a new offer with "(.*?)" as the title$/) do |title|
  visit '/job_offers/new'
  @title = title
  fill_in('job_offer_form[title]', with: title)
  click_button('Create')
end

Then(/^I should see a offer created confirmation message$/) do
  page.should have_content(OFFER_CREATED_MESSAGE)
end

Then(/^I should see a offer updated confirmation message$/) do
  page.should have_content(OFFER_UPDATED_MESSAGE)
end

Then(/^I should see a offer deleted confirmation message$/) do
  page.should have_content(OFFER_DELETED_MESSAGE)
end

Then(/^I should see "(.*?)" in my offers list$/) do |content|
  visit '/job_offers/my'
  page.should have_content(content)
end

Then(/^I should not see "(.*?)" in my offers list$/) do |content|
  visit '/job_offers/my'
  page.should_not have_content(content)
end

Then(/^I should see a registration menu$/) do
  page.should have_content(REGISTRATION_MENU)
end

Then(/^I should see a job offers menu$/) do
  page.should have_content(JOB_OFFERS_MENU)
end

Given(/^I have "(.*?)" offer in my offers list$/) do |offer_title|
  JobOfferRepository.new.delete_all

  visit '/job_offers/new'
  @title = offer_title
  fill_in('job_offer_form[title]', with: offer_title)
  click_button('Create')
end

When(/^I change the title to "(.*?)"$/) do |new_title|
  click_link('Edit')
  fill_in('job_offer_form[title]', with: new_title)
  click_button('Save')
end

And(/^I delete it$/) do
  click_button('Delete')
end

When('I create a new offer with {string} as the title and {string} as the experience required') do |title, experience_required|
  visit '/job_offers/new'
  @title = title
  fill_in('job_offer_form[title]', with: title)
  fill_in('job_offer_form[experience_required]', with: experience_required)
  click_button('Create')
end

Then('I should see {string} in the experience required of this offer in my offers list') do |experience_required|
  visit '/job_offers/my'
  td = page.find(:css, 'td', text: @title)
  tr = td.find(:xpath, './parent::tr')
  tr.should have_content(experience_required)
end

When('I activate it') do
  # td = page.find(:css, 'td', text: @title)
  # tr = td.find(:xpath, './parent::tr')
  # button = tr.find(:css, )
  click_button('Activate')
end

Then('I should see an offer activated confirmation message') do
  page.should have_content(OFFER_ACTIVATED_MESSAGE)
end

Then('I should see the offer in Job offers list') do
  visit '/job_offers/latest'
  page.should have_content(@title)
end

Given('I have {string} offer with {string} as experience required in my offers list') do |title, experience_required|
  JobOfferRepository.new.delete_all

  visit '/job_offers/new'
  @title = title
  fill_in('job_offer_form[title]', with: title)
  fill_in('job_offer_form[experience_required]', with: experience_required)
  click_button('Create')
end

Then('I should see the offer in Job offers list with {string} as the experience required') do |experience_required|
  visit '/job_offers/latest'
  td = page.find(:css, 'td', text: @title)
  tr = td.find(:xpath, './parent::tr')
  tr.should have_content(experience_required)
end

Given('I have {string} offer without specifying the experience required in my offers list') do |title|
  JobOfferRepository.new.delete_all

  visit '/job_offers/new'
  @title = title
  fill_in('job_offer_form[title]', with: title)
  click_button('Create')
end
