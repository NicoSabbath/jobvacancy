require 'spec_helper'

describe JobOffer do
  describe 'valid?' do
    it 'should be invalid when title is blank' do
      check_validation(:title, "Title can't be blank") do
        described_class.new(location: 'a location')
      end
    end

    it 'should be valid when title is not blank' do
      job_offer = described_class.new(title: 'a title')
      expect(job_offer).to be_valid
    end

    it 'should be invalid when experience is a negative number' do
      check_validation(:experience_required, 'Experience required must be greater than or equal to 0') do
        described_class.new(title: 'a title', experience_required: -1)
      end
    end
  end
end
